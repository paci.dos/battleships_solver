# Importing Image class from PIL module
from PIL import Image

def crop_and_save_img(name,number):
    im = Image.open(rf"C:\Users\User\Desktop\python\android\random_gen_bigger_sample\{name}")
    
    left = 310
    top = 240
    right = 1113
    bottom = 1040
    
    im1 = im.crop((left, top, right, bottom))
    
    new_name = f"{number}.png"

    im1.save(new_name)
    #im1.show()
    print(new_name)

if __name__ == '__main__':

    for i in range(1971, 6179):
        name = f"screen_{i}.png"
        crop_and_save_img(name, i)