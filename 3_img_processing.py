import cv2
import numpy as np
from numpy.linalg import matrix_rank
import glob
from sqlalchemy import create_engine
from sqlalchemy import Table, Column, Integer, String, MetaData
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker

engine = create_engine('sqlite:///seabattle.db', echo = False)
Base = declarative_base()

class Layout(Base):
    __tablename__ = 'layout'

    id = Column(Integer, primary_key=True)
    cell_0 = Column(Integer) 
    cell_1 = Column(Integer)
    cell_2 = Column(Integer) 
    cell_3 = Column(Integer)
    cell_4 = Column(Integer) 
    cell_5 = Column(Integer)
    cell_6 = Column(Integer) 
    cell_7 = Column(Integer)
    cell_8 = Column(Integer) 
    cell_9 = Column(Integer)
    cell_10 = Column(Integer) 
    cell_11 = Column(Integer)
    cell_12 = Column(Integer) 
    cell_13 = Column(Integer)
    cell_14 = Column(Integer) 
    cell_15 = Column(Integer)
    cell_16 = Column(Integer) 
    cell_17 = Column(Integer)
    cell_18 = Column(Integer) 
    cell_19 = Column(Integer)
    cell_20 = Column(Integer) 
    cell_21 = Column(Integer)
    cell_22 = Column(Integer) 
    cell_23 = Column(Integer)
    cell_24 = Column(Integer) 
    cell_25 = Column(Integer)
    cell_26 = Column(Integer) 
    cell_27 = Column(Integer)
    cell_28 = Column(Integer) 
    cell_29 = Column(Integer)
    cell_30 = Column(Integer) 
    cell_31 = Column(Integer)
    cell_32 = Column(Integer) 
    cell_33 = Column(Integer)
    cell_34 = Column(Integer) 
    cell_35 = Column(Integer)
    cell_36 = Column(Integer) 
    cell_37 = Column(Integer)
    cell_38 = Column(Integer) 
    cell_39 = Column(Integer)
    cell_40 = Column(Integer)
    cell_41 = Column(Integer)
    cell_42 = Column(Integer) 
    cell_43 = Column(Integer)
    cell_44 = Column(Integer) 
    cell_45 = Column(Integer)
    cell_46 = Column(Integer) 
    cell_47 = Column(Integer)
    cell_48 = Column(Integer) 
    cell_49 = Column(Integer)
    cell_50 = Column(Integer) 
    cell_51 = Column(Integer)
    cell_52 = Column(Integer) 
    cell_53 = Column(Integer)
    cell_54 = Column(Integer) 
    cell_55 = Column(Integer)
    cell_56 = Column(Integer) 
    cell_57 = Column(Integer)
    cell_58 = Column(Integer) 
    cell_59 = Column(Integer)
    cell_60 = Column(Integer) 
    cell_61 = Column(Integer)
    cell_62 = Column(Integer) 
    cell_63 = Column(Integer)
    cell_64 = Column(Integer) 
    cell_65 = Column(Integer)
    cell_66 = Column(Integer) 
    cell_67 = Column(Integer)
    cell_68 = Column(Integer) 
    cell_69 = Column(Integer)
    cell_70 = Column(Integer) 
    cell_71 = Column(Integer)
    cell_72 = Column(Integer) 
    cell_73 = Column(Integer)
    cell_74 = Column(Integer) 
    cell_75 = Column(Integer)
    cell_76 = Column(Integer) 
    cell_77 = Column(Integer)
    cell_78 = Column(Integer) 
    cell_79 = Column(Integer)
    cell_80 = Column(Integer) 
    cell_81 = Column(Integer)
    cell_82 = Column(Integer) 
    cell_83 = Column(Integer)
    cell_84 = Column(Integer) 
    cell_85 = Column(Integer)
    cell_86 = Column(Integer) 
    cell_87 = Column(Integer)
    cell_88 = Column(Integer) 
    cell_89 = Column(Integer)
    cell_90 = Column(Integer) 
    cell_91 = Column(Integer)
    cell_92 = Column(Integer) 
    cell_93 = Column(Integer)
    cell_94 = Column(Integer) 
    cell_95 = Column(Integer)
    cell_96 = Column(Integer) 
    cell_97 = Column(Integer)
    cell_98 = Column(Integer) 
    cell_99 = Column(Integer)

Base.metadata.create_all(engine)

def insert_layout_to_db(list_of_cells):
    
    Session = sessionmaker(bind = engine)
    session = Session()

    query_dict = {}

    for key, val in enumerate(list_of_cells):
        cell = 'cell_' + str(key)
        query_dict.update({cell : val})

    duplicit_record = get_record(query_dict)
    if duplicit_record != True:
        session.execute(Layout.__table__.insert(), query_dict)
        session.commit()

def get_record(record):
    Session = sessionmaker(bind = engine)
    session = Session()

    query = session.query(Layout)
    for attr, value in record.items():
        query = query.filter(getattr(Layout, attr).like("%%%s%%" % value))

    results = query.all()

    if len(results) > 1:
        return True
    else:
        return False

def main():
    
    ROW_POSITIONS = [50, 130, 205, 280, 360, 435, 515, 595, 673, 750]
    COL_POSITIONS = [50, 130, 205, 285, 360, 437, 515, 595, 673, 750]
    sum_matrix = np.array(([0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                           [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                           [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                           [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                           [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                           [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                           [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                           [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                           [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                           [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]))

    for image in glob.glob("random_gen/*.png"):

        im_gray = cv2.imread(image, cv2.IMREAD_GRAYSCALE)
        (thresh, im_bw) = cv2.threshold(im_gray, 128, 255, cv2.THRESH_BINARY | cv2.THRESH_OTSU)
        thresh = 254
        im_bw = cv2.threshold(im_gray, thresh, 255, cv2.THRESH_BINARY)[1]
        np_image = np.array(im_bw)

        cells_status = []
        ranks_matrix = []
        for row_dot in ROW_POSITIONS:
            ranks_in_row = []
            for col_dot in COL_POSITIONS:
                #cv2.circle(im_bw, (row_dot,col_dot), 5, (0, 0, 255), -1)
                #print(f"({row_dot},{col_dot})")
                slice = np_image[row_dot-28:row_dot+28, col_dot-28:col_dot+28]
                rank = matrix_rank(slice)
                if rank == 1:
                    ranks_in_row.append(0)
                    cells_status.append(0)
                else:
                    ranks_in_row.append(1)
                    cells_status.append(1)

            ranks_matrix.append(ranks_in_row)

        count = sum(x.count(1) for x in ranks_matrix)
        if count == 20:
            sum_matrix = sum_matrix + np.array(ranks_matrix)
            insert_layout_to_db(cells_status)
            #print(image)
    print(sum_matrix)

if __name__ == '__main__':
    
    main()
