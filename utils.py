import csv
import re

def count_probability():

    list_of_probabilities = []
    with open('sum.csv', newline='') as csvfile:
        spamreader = csv.reader(csvfile, delimiter=' ', quotechar='|')
        for row in spamreader:
            row_probability = []
            for item in row:
                item = re.sub(r"\[|\]", "", item)
                if len(item) > 0:
                    probability = (int(item) / 3)*100
                    item = round(probability, 1)
                    row_probability.append(item)
                    list_of_probabilities.append(item)
            print(row_probability)

    list_of_probabilities.sort(reverse=True)
    print(list_of_probabilities)     


if __name__ == '__main__':
    count_probability()