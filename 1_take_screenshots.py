from ppadb.client import Client as AdbClient
import time

number_of_screenshots = 0

def connect():
    client = AdbClient(host="127.0.0.1", port=5037) # Default is "127.0.0.1" and 5037

    devices = client.devices()

    if len(devices) == 0:
        print('No devices')
        quit()

    device = devices[0]

    print(f'Connected to {device}')

    return device, client


def screen(device, number_of_screenshots):

    name = f"screen_{number_of_screenshots}.png"

    screenshot = device.screencap()

    with open(name, 'wb') as f: # save the screenshot as result.png
        f.write(screenshot)
        print(f"screen name: {name}")


if __name__ == '__main__':
    device, client = connect()
    
    
    for i in range(1001):
        device.shell('input tap 1245 960')
        time.sleep(0.2)
        screen(device, i)
