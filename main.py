import PySimpleGUI as sg
import os
from sqlalchemy import create_engine
from sqlalchemy import Table, Column, Integer, String, MetaData
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker

engine = create_engine('sqlite:///seabattle.db', echo = False)
Base = declarative_base()

class Layout(Base):
    __tablename__ = 'layout'

    id = Column(Integer, primary_key=True)
    cell_0 = Column(Integer) 
    cell_1 = Column(Integer)
    cell_2 = Column(Integer) 
    cell_3 = Column(Integer)
    cell_4 = Column(Integer) 
    cell_5 = Column(Integer)
    cell_6 = Column(Integer) 
    cell_7 = Column(Integer)
    cell_8 = Column(Integer) 
    cell_9 = Column(Integer)
    cell_10 = Column(Integer) 
    cell_11 = Column(Integer)
    cell_12 = Column(Integer) 
    cell_13 = Column(Integer)
    cell_14 = Column(Integer) 
    cell_15 = Column(Integer)
    cell_16 = Column(Integer) 
    cell_17 = Column(Integer)
    cell_18 = Column(Integer) 
    cell_19 = Column(Integer)
    cell_20 = Column(Integer) 
    cell_21 = Column(Integer)
    cell_22 = Column(Integer) 
    cell_23 = Column(Integer)
    cell_24 = Column(Integer) 
    cell_25 = Column(Integer)
    cell_26 = Column(Integer) 
    cell_27 = Column(Integer)
    cell_28 = Column(Integer) 
    cell_29 = Column(Integer)
    cell_30 = Column(Integer) 
    cell_31 = Column(Integer)
    cell_32 = Column(Integer) 
    cell_33 = Column(Integer)
    cell_34 = Column(Integer) 
    cell_35 = Column(Integer)
    cell_36 = Column(Integer) 
    cell_37 = Column(Integer)
    cell_38 = Column(Integer) 
    cell_39 = Column(Integer)
    cell_40 = Column(Integer)
    cell_41 = Column(Integer)
    cell_42 = Column(Integer) 
    cell_43 = Column(Integer)
    cell_44 = Column(Integer) 
    cell_45 = Column(Integer)
    cell_46 = Column(Integer) 
    cell_47 = Column(Integer)
    cell_48 = Column(Integer) 
    cell_49 = Column(Integer)
    cell_50 = Column(Integer) 
    cell_51 = Column(Integer)
    cell_52 = Column(Integer) 
    cell_53 = Column(Integer)
    cell_54 = Column(Integer) 
    cell_55 = Column(Integer)
    cell_56 = Column(Integer) 
    cell_57 = Column(Integer)
    cell_58 = Column(Integer) 
    cell_59 = Column(Integer)
    cell_60 = Column(Integer) 
    cell_61 = Column(Integer)
    cell_62 = Column(Integer) 
    cell_63 = Column(Integer)
    cell_64 = Column(Integer) 
    cell_65 = Column(Integer)
    cell_66 = Column(Integer) 
    cell_67 = Column(Integer)
    cell_68 = Column(Integer) 
    cell_69 = Column(Integer)
    cell_70 = Column(Integer) 
    cell_71 = Column(Integer)
    cell_72 = Column(Integer) 
    cell_73 = Column(Integer)
    cell_74 = Column(Integer) 
    cell_75 = Column(Integer)
    cell_76 = Column(Integer) 
    cell_77 = Column(Integer)
    cell_78 = Column(Integer) 
    cell_79 = Column(Integer)
    cell_80 = Column(Integer) 
    cell_81 = Column(Integer)
    cell_82 = Column(Integer) 
    cell_83 = Column(Integer)
    cell_84 = Column(Integer) 
    cell_85 = Column(Integer)
    cell_86 = Column(Integer) 
    cell_87 = Column(Integer)
    cell_88 = Column(Integer) 
    cell_89 = Column(Integer)
    cell_90 = Column(Integer) 
    cell_91 = Column(Integer)
    cell_92 = Column(Integer) 
    cell_93 = Column(Integer)
    cell_94 = Column(Integer) 
    cell_95 = Column(Integer)
    cell_96 = Column(Integer) 
    cell_97 = Column(Integer)
    cell_98 = Column(Integer) 
    cell_99 = Column(Integer)

@classmethod
def get_atr(self, attr):
        return self[attr]

Base.metadata.create_all(engine)

map_filter = {
    "(0, 0)" : "cell_0",
    "(0, 1)" : "cell_1",
    "(0, 2)" : "cell_2",
    "(0, 3)" : "cell_3",
    "(0, 4)" : "cell_4",
    "(0, 5)" : "cell_5",
    "(0, 6)" : "cell_6",
    "(0, 7)" : "cell_7",
    "(0, 8)" : "cell_8",
    "(0, 9)" : "cell_9",
    "(1, 0)" : "cell_10",
    "(1, 1)" : "cell_11",
    "(1, 2)" : "cell_12",
    "(1, 3)" : "cell_13",
    "(1, 4)" : "cell_14",
    "(1, 5)" : "cell_15",
    "(1, 6)" : "cell_16",
    "(1, 7)" : "cell_17",
    "(1, 8)" : "cell_18",
    "(1, 9)" : "cell_19",
    "(2, 0)" : "cell_20",
    "(2, 1)" : "cell_21",
    "(2, 2)" : "cell_22",
    "(2, 3)" : "cell_23",
    "(2, 4)" : "cell_24",
    "(2, 5)" : "cell_25",
    "(2, 6)" : "cell_26",
    "(2, 7)" : "cell_27",
    "(2, 8)" : "cell_28",
    "(2, 9)" : "cell_29",
    "(3, 0)" : "cell_30",
    "(3, 1)" : "cell_31",
    "(3, 2)" : "cell_32",
    "(3, 3)" : "cell_33",
    "(3, 4)" : "cell_34",
    "(3, 5)" : "cell_35",
    "(3, 6)" : "cell_36",
    "(3, 7)" : "cell_37",
    "(3, 8)" : "cell_38",
    "(3, 9)" : "cell_39",
    "(4, 0)" : "cell_40",
    "(4, 1)" : "cell_41",
    "(4, 2)" : "cell_42",
    "(4, 3)" : "cell_43",
    "(4, 4)" : "cell_44",
    "(4, 5)" : "cell_45",
    "(4, 6)" : "cell_46",
    "(4, 7)" : "cell_47",
    "(4, 8)" : "cell_48",
    "(4, 9)" : "cell_49",
    "(5, 0)" : "cell_50",
    "(5, 1)" : "cell_51",
    "(5, 2)" : "cell_52",
    "(5, 3)" : "cell_53",   
    "(5, 4)" : "cell_54",   
    "(5, 5)" : "cell_55",   
    "(5, 6)" : "cell_56",   
    "(5, 7)" : "cell_57",   
    "(5, 8)" : "cell_58",   
    "(5, 9)" : "cell_59",   
    "(6, 0)" : "cell_60",   
    "(6, 1)" : "cell_61",   
    "(6, 2)" : "cell_62",   
    "(6, 3)" : "cell_63",   
    "(6, 4)" : "cell_64",   
    "(6, 5)" : "cell_65",   
    "(6, 6)" : "cell_66",   
    "(6, 7)" : "cell_67",   
    "(6, 8)" : "cell_68",   
    "(6, 9)" : "cell_69",   
    "(7, 0)" : "cell_70",   
    "(7, 1)" : "cell_71",   
    "(7, 2)" : "cell_72",   
    "(7, 3)" : "cell_73",   
    "(7, 4)" : "cell_74",   
    "(7, 5)" : "cell_75",   
    "(7, 6)" : "cell_76",   
    "(7, 7)" : "cell_77",   
    "(7, 8)" : "cell_78",   
    "(7, 9)" : "cell_79",   
    "(8, 0)" : "cell_80",   
    "(8, 1)" : "cell_81",   
    "(8, 2)" : "cell_82",   
    "(8, 3)" : "cell_83",   
    "(8, 4)" : "cell_84",   
    "(8, 5)" : "cell_85",   
    "(8, 6)" : "cell_86",   
    "(8, 7)" : "cell_87",   
    "(8, 8)" : "cell_88",   
    "(8, 9)" : "cell_89",   
    "(9, 0)" : "cell_90",   
    "(9, 1)" : "cell_91",   
    "(9, 2)" : "cell_92",   
    "(9, 3)" : "cell_93",   
    "(9, 4)" : "cell_94",   
    "(9, 5)" : "cell_95",   
    "(9, 6)" : "cell_96",   
    "(9, 7)" : "cell_97",   
    "(9, 8)" : "cell_98",   
    "(9, 9)" : "cell_99"
}


def get_record(record):

    clear = lambda: os.system('cls')
    clear()

    Session = sessionmaker(bind = engine)
    session = Session()

    query = session.query(Layout)
    for attr, value in record.items():
        query = query.filter(getattr(Layout, attr).like("%%%s%%" % value))

    result_exist = query.first() is not None
    result = query.first()
    
    if result_exist == True:
        result_dict = vars(result)

        row = []
        for i in range(100):
            row.append(result_dict[f'cell_{i}'])
            if i in (9,19,29,39,49,59,69,79,89,99):
                print(row)
                row = []
    else:
        print("No result")

def get_column(cell):
    return map_filter.get(str(cell))

def main():
    ships = set()
    water = set()
    MAX_ROWS = MAX_COL = 10
    sg.theme('LightGreen5')

    layout =  [[sg.Button('', size=(4, 2), key=(i,j), pad=(0,0), button_color=('grey')) for j in range(MAX_COL)] for i in range(MAX_ROWS)]

    window = sg.Window('SeaBattle solver', layout)

    query_dict = {}
    while True:
        event, values = window.read()
        if event in (sg.WIN_CLOSED, 'Exit'):
            break
        
        window[event].update(button_color=('lightblue'))
        if event in water:
            window[event].update(button_color=('red'))
            ships.add(event)
            water.remove(event)
            key = get_column(event)
            query_dict.update({key : 1})
            #print(f"{key} - 1")
        elif event in ships:
            window[event].update(button_color=('grey'))
            ships.remove(event)
            key = get_column(event)
            query_dict.update({key : 0})
            #print(f"{key} - 0")
        else:
            water.add(event)
            key = get_column(event)
            query_dict.update({key : 0})
            #print(f"{key} - 0")

        # print(f"event: {event}")
        # print(f"water: {water}")
        # print(f"ships: {ships}")
        # print("************************")
        get_record(query_dict)

    window.close()

if __name__ == '__main__':
    
    main()